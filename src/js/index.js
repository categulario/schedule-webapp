/**
 * Returns HH:MM of the current time
 */
function strtime() {
  const date = new Date();
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');

  return `${hours}:${minutes}`;
}

/**
 * Returns YYYY-mm-dd of the current time:
 */
function strdate() {
  return (new Date()).toISOString().slice(0, 10);
}

/**
 * Takes a string and stripts common accents from letters.
 */
function normalize(s) {
  var r = s.toLowerCase();

  r = r.replace(new RegExp(/[àáâãäå]/g),"a");
  r = r.replace(new RegExp(/[èéêë]/g),"e");
  r = r.replace(new RegExp(/[ìíîï]/g),"i");
  r = r.replace(new RegExp(/ñ/g),"n");
  r = r.replace(new RegExp(/[òóôõö]/g),"o");
  r = r.replace(new RegExp(/[ùúûü]/g),"u");

  return r;
}

window.App = new Vue({
  data: {
    title: 'Find talks',
    schedule: [],
    talk: {},
    marks: [
      "09:00",
      "09:45",
      "10:25",
      "10:50",
      "11:25",
      "13:00",
      "14:00",
      "14:15",
      "14:40",
      "15:30",
      "15:55",
      "16:20",
      "16:30",
    ],
    last_checksum: '',
    section: 'home',
    left_icon: 'fa-bars',
    left_action: null,
    currentHour: strtime(),
    searchterm: '',

    last_section: '',
    last_sub: '',
    sections: {
      home: {
        title: 'Find talks',
        left_icon: 'fa-bars',
        left_action: null,
      },
      talk: {
        title: 'Talk',
        left_icon: 'fa-arrow-left',
        left_action: 'back',
      },
      schedule: {
        title: 'Talks',
        left_icon: 'fa-arrow-left',
        left_action: 'home',

        subs: {
          now: {
            title: 'Charlas ahora',
            default_filter: 'talksHappeningNow',
          },
          next: {
            title: 'Charlas siguientes',
            default_filter: 'talksHappeningNext',
          },
          today: {
            title: 'Programa hoy',
            default_filter: 'talksToday',
          },
          find: {
            title: 'Search everywhere',
            default_filter: 'all',
          },
          star: {
            title: 'Mis pláticas favoritas',
            default_filter: 'starFilter',
          },

          // Routes by day
          day1: { title: 'Programa Lunes', default_filter: 'talksDay1' },
          day2: { title: 'Programa Martes', default_filter: 'talksDay2' },
          day3: { title: 'Programa Miércoles', default_filter: 'talksDay3' },
          day4: { title: 'Programa Jueves', default_filter: 'talksDay4' },
          day5: { title: 'Programa Viernes', default_filter: 'talksDay5' },
        },
      },
    },
  },

  mounted: function () {
    this.setLoader('Iniciando la magia...');

    var last_time = localStorage.getItem('last_update');

    if (!last_time) {
      this.setLoader('No tengo el horario, descargando...');
      this.loadData();
    } else {
      this.setLoader('Me se el horario de memoria, cargando...');
      this.schedule = this.recoverPersistedSchedule();
      this.unsetLoader();
    }
  },

  created: function () {
    setInterval(function () {
      this.currentHour = strtime();
    }.bind(this), 2000);
  },

  computed: {
    talksHappeningNow: function () {
      var now = this.currentHour;
      var day = strdate();

      return this.schedule.filter((talk) => {
        return talk.day == day && talk.from <= now && talk.to > now;
      });
    },

    nextTime: function () {
      var now = this.currentHour;

      return this.marks.find(item => item > now);
    },

    talksHappeningNext: function () {
      var day = strdate();
      var nextTime = this.nextTime;

      return this.schedule.filter((talk) => {
        return talk.day == day && talk.from <= nextTime && talk.to > nextTime;
      });
    },

    talksToday: function () {
      var day = strdate();

      return this.schedule.filter((talk) => {
        return talk.day == day;
      });
    },

    all: function () {
      return this.schedule;
    },

    starFilter: function () {
      return this.schedule.filter(function (talk) {
        return talk.star;
      });
    },

    // filters by day
    talksDay1: function () { return this.schedule.filter((talk) => talk.day == 'lunes') },
    talksDay2: function () { return this.schedule.filter((talk) => talk.day == 'martes') },
    talksDay3: function () { return this.schedule.filter((talk) => talk.day == 'miercoles') },
    talksDay4: function () { return this.schedule.filter((talk) => talk.day == 'jueves') },
    talksDay5: function () { return this.schedule.filter((talk) => talk.day == 'viernes') },
  },

  methods: {
    randomMessage: function () {
      var max = 9;
      return [
        'aren\'t you hungry?',
        'you better go explore the city',
        'time to organize a football match',
        'time to learn a new programming language',
        'time to make friends from other parts of the world',
        'time to learn esperanto',
        'perfect timing for climbing a tree',
        'did you buy any souvenirs?',
        'time to install Linux!',
      ][Math.floor(Math.random() * max)];
    },

    filterSearch: function (item) {
      var st = normalize(this.searchterm);

      if (!st) {
        return true;
      } else {
        return item.blob.indexOf(st) >= 0;
      }
    },

    persistSchedule: function () {
      this.schedule.forEach(function (item) {
        localStorage.setItem('item:'+item.id, JSON.stringify(item));
      });

      localStorage.setItem('items', JSON.stringify(this.schedule.map(function (i) {
        return i.id;
      })));

      localStorage.setItem('last_update', (new Date()).toISOString());
    },

    recoverPersistedSchedule: function () {
      return JSON.parse(localStorage.getItem('items')).map(function (i) {
        return JSON.parse(localStorage.getItem('item:'+i));
      });
    },

    setLoader: function (msg) {
      document.getElementById('loader-message').innerHTML = msg;
      document.getElementById('loader').className = 'on';
    },

    unsetLoader: function () {
      document.getElementById('loader').className = 'off';
    },

    talksNowClick: function () {
      this.changeSection('schedule', 'now');
    },

    talksNextClick: function () {
      this.changeSection('schedule', 'next');
    },

    scheduleClick: function () {
      this.changeSection('schedule', 'today');
    },

    findTalksClick: function () {
      this.changeSection('schedule', 'find');
    },

    clearSearch: function () {
      this.searchterm = '';
    },

    runLeftAction: function () {
      if (this.left_action === null) {
        this.showPanel();
        return;
      }

      this.changeSection(this.left_action);
    },

    scheduleByDay: function (event) {
      var day = event.target.dataset.day;

      this.changeSection('schedule', `day${day}`);
    },

    scheduleByStar: function () {
      this.changeSection('schedule', 'star');
    },

    scheduleByArea: function (event) {
      var area = event.target.dataset.area;

      this.changeSection('schedule', `area${area}`);
    },

    toggleStar: function (event) {
      event.stopPropagation();
      var id = event.currentTarget.dataset.id;

      this.schedule = this.schedule.map(function (talk) {
        if (talk.id == id) {
          talk.star = ! talk.star;
        }

        return talk;
      });

      this.persistSchedule();
    },

    reloadSchedule: function () {
      this.loadData();
    },

    loadData: async function () {
      this.setLoader('Descargando horario...');
      var starred = this.schedule.filter(function (t) {
        return t.star;
      }).map(function (t) {
        return t.id;
      });

      let resp;

      try {
        const response = await fetch('talks.json');
        resp = await response.json();
      } catch (err) {
        this.unsetLoader();
        console.log(err);
        alert('Error de conexión');
      }

      var ponencias = [];

      resp.forEach(function (ponencia) {
        var blob = '';

        [
          "place",
          "area",
          "title",
          "author",
        ].forEach(function (prop) {
          blob += ' - ' + (ponencia[prop] || '');
        });

        ponencia.blob = normalize(blob);
        ponencia.star = starred.indexOf(ponencia.id)>=0;

        ponencias.push(ponencia);
      }.bind(this));

      ponencias.sort(function (a, b) {
        if (a.day == b.day) {
          return a.from > b.from;
        }

        return a.day > b.day;
      }.bind(this));

      this.schedule = ponencias;

      this.setLoader('Guardando información...');
      this.persistSchedule();
      this.unsetLoader();
    },

    showTalk: function (event) {
      var id = event.currentTarget.dataset.id;

      this.talk = this.schedule.filter((t) => {
        return t.id == id;
      })[0];
      this.changeSection('talk');
    },

    showPanel: function () {
      document.getElementById('left-panel').classList = [];
      document.getElementById('overlay').classList = [];
    },

    hidePanel: function () {
      document.getElementById('left-panel').className = "off";
      document.getElementById('overlay').className = "off";
    },

    changeSection: function (section, sub) {
      this.hidePanel();

      if (section == 'back') {
        return this.changeSection(this.last_section, this.last_sub);
      }

      this.last_section = this.section;
      this.last_sub = this.subsection;
      this.section = section;
      this.subsection = sub;
      this.title = this.sections[section].title;
      this.left_icon = this.sections[section].left_icon;
      this.left_action = this.sections[section].left_action;

      if (this.sections[section].subs && sub) {
        this.title = this.sections[section].subs[sub].title;
        this.default_filter = this.sections[section].subs[sub].default_filter;
      }
    },
  },
});

document.addEventListener('DOMContentLoaded', function () {
  App.$mount('#app');
}, false);
